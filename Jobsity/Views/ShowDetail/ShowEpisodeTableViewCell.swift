//
//  ShowEpisodeTableViewCell.swift
//  Jobsity
//
//  Created by Arthur Henrique de Oliveira on 09/05/21.
//

import UIKit

class ShowEpisodeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    func setData(_ data: Episode) {
        if let url = URL(string: data.image?.medium ?? "") {
            posterImageView.downloadImage(from: url)
        }
        nameLabel.text = data.name
        timeLabel.text = "\(data.runtime!) min"
        descriptionLabel.text = data.summary?.htmlFormat
    }
}
