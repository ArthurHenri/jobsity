//
//  ShowDetailTableViewCell.swift
//  Jobsity
//
//  Created by Arthur Henrique de Oliveira on 09/05/21.
//

import UIKit

class ShowDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var scheduleTimeLabel: UILabel!
    @IBOutlet weak var scheduleDaysLabel: UILabel!
    @IBOutlet weak var genresLabel: UILabel!
    @IBOutlet weak var summaryLabel: UILabel!
    @IBOutlet weak var seasonCollectionViewCell: SeasonCollectionView!
    
    var delegate: ShowDetailTableViewDelegate? {
        didSet {
            seasonCollectionViewCell.showDetailDelegate = delegate
        }
    }
    
    func setData(_ data: Show) {
        if let url = URL(string: data.image?.medium ?? "") {
            posterImageView.downloadImage(from: url)
        }
        nameLabel.text = data.name
        if let scheduleTime = data.schedule?.time, !scheduleTime.isEmpty {
            scheduleTimeLabel.text = "🕐 " + scheduleTime
        }
        if let scheduleDays = data.schedule?.getDaysFormated(), !scheduleDays.isEmpty {
            scheduleDaysLabel.text = "🗓 " + scheduleDays
        }
        genresLabel.text = data.getFormattedGenres()
        summaryLabel.text = data.summary?.htmlFormat
    }
    
    func setNumberOfSeasons(_ number: Int) {
        seasonCollectionViewCell.numberOfSeasons = number
    }
}
