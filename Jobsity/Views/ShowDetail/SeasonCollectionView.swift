//
//  SeasonCollectionView.swift
//  Jobsity
//
//  Created by Arthur Henrique de Oliveira on 12/05/21.
//

import UIKit

class SeasonCollectionView: UICollectionView {
    
    var showDetailDelegate: ShowDetailTableViewDelegate?
    
    var numberOfSeasons = 0 {
        didSet {
            reloadData()
        }
    }
    
    private var selectedSeason = 0
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        register(UINib(nibName: "SeasonCollectionViewCell", bundle: nil),
                 forCellWithReuseIdentifier: "cell")
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        
        collectionViewLayout = layout
        
        dataSource = self
        delegate = self
    }
}

extension SeasonCollectionView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numberOfSeasons
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SeasonCollectionViewCell
        cell.nameLabel.text = "Season \(indexPath.row + 1)"
        cell.nameLabel.textColor = indexPath.row == selectedSeason ? UIColor.white : UIColor.black
        cell.backgroundColor = indexPath.row == selectedSeason ? UIColor.darkGray : UIColor.systemGray4
        return cell
    }
}

extension SeasonCollectionView: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let cell = Bundle.main.loadNibNamed("SeasonCollectionViewCell",
                                                  owner: self,
                                                  options: nil)?.first as? SeasonCollectionViewCell else {
            return CGSize.zero
        }
        cell.nameLabel.text = "Season \(indexPath.row + 1)"
        
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        
        let size: CGSize = cell.contentView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        
        return CGSize(width: size.width, height: frame.height)
    }
}

extension SeasonCollectionView: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedSeason = indexPath.row
        showDetailDelegate?.didSelectSeason(indexPath.row + 1)
        reloadData()
    }
}
