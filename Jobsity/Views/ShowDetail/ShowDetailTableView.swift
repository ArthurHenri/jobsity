//
//  ShowDetailTableView.swift
//  Jobsity
//
//  Created by Arthur Henrique de Oliveira on 09/05/21.
//

import UIKit

protocol ShowDetailTableViewDataSource {
    func getShow() -> Show
    func getEpisode(fromIndex index: Int) -> Episode
    func getNumberOfEpisodes() -> Int
    func getNumberOfSeasons() -> Int
}

protocol ShowDetailTableViewDelegate {
    func didSelectEpisode(fromIndex index: Int)
    func didSelectSeason(_ season: Int)
}

class ShowDetailTableView: UITableView {
    
    var showDetailDataSource: ShowDetailTableViewDataSource?
    var showDetailDelegate: ShowDetailTableViewDelegate?

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        register(UINib(nibName: "ShowDetailTableViewCell", bundle: nil),
                 forCellReuseIdentifier: "detail_cell")
        
        register(UINib(nibName: "ShowEpisodeTableViewCell", bundle: nil),
                 forCellReuseIdentifier: "episode_cell")
        
        
        dataSource = self
        delegate = self
    }
}

extension ShowDetailTableView: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == Constants.SHOW_DETAIL_ROW {
            return 1
        }
        return showDetailDataSource?.getNumberOfEpisodes() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == Constants.SHOW_DETAIL_ROW {
            return detailCellForRowAt(indexPath)
        } else {
            return episodeCellForRowAt(indexPath)
        }
    }
    
    private func detailCellForRowAt(_ indexPath: IndexPath) -> UITableViewCell {
        let cell = dequeueReusableCell(withIdentifier: "detail_cell", for: indexPath) as! ShowDetailTableViewCell
        if let show = showDetailDataSource?.getShow() {
            cell.setData(show)
            cell.setNumberOfSeasons(showDetailDataSource?.getNumberOfSeasons() ?? 1)
            cell.delegate = showDetailDelegate
        }
        return cell
    }
    
    private func episodeCellForRowAt(_ indexPath: IndexPath) -> UITableViewCell {
        let cell = dequeueReusableCell(withIdentifier: "episode_cell", for: indexPath) as! ShowEpisodeTableViewCell
        if let episode = showDetailDataSource?.getEpisode(fromIndex: indexPath.row) {
            cell.setData(episode)
        }
        return cell
    }
}

extension ShowDetailTableView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section != Constants.SHOW_DETAIL_ROW {
            showDetailDelegate?.didSelectEpisode(fromIndex: indexPath.row)
        }
    }
}
