//
//  ShowsCollectionViewCell.swift
//  Jobsity
//
//  Created by Arthur Henrique de Oliveira on 09/05/21.
//

import UIKit

class ShowsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    
    func setData(_ data: Show) {
        nameLabel.text = data.name
        if let url = URL(string: data.image?.medium ?? "") {
            posterImageView.downloadImage(from: url)
        }
        var infoString = ""
        if let rating = data.rating?.average {
            infoString = "⭑ \(rating)"
        }
        if let networkName = data.network?.name {
            infoString += " • \(networkName)"
        }
        infoLabel.text = infoString
    }
}
