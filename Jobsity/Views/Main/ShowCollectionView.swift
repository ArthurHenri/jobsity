//
//  ShowCollectionView.swift
//  Jobsity
//
//  Created by Arthur Henrique de Oliveira on 09/05/21.
//

import UIKit

protocol ShowCollectionViewDelegate {
    func didSelectShow(_ show: Show)
    func didReachEnd()
}

class ShowCollectionView: UICollectionView {
    
    private var shows = [Show]()
    
    var showDelegate: ShowCollectionViewDelegate?
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        register(UINib(nibName: "ShowsCollectionViewCell", bundle: nil),
                 forCellWithReuseIdentifier: "cell")
        
        dataSource = self
        delegate = self
    }
    
    func setData(_ data: [Show]) {
        shows = data
        
        reloadData()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        superview?.endEditing(true)
        if contentOffset.y >= (contentSize.height - frame.size.height) {
            showDelegate?.didReachEnd()
        }
    }
}

extension ShowCollectionView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return shows.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ShowsCollectionViewCell
        
        cell.setData(shows[indexPath.row])
        
        return cell
    }
}

extension ShowCollectionView: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/2, height: collectionView.frame.width/1.5)
    }
}

extension ShowCollectionView: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let show = shows[indexPath.row]
        showDelegate?.didSelectShow(show)
    }
}
