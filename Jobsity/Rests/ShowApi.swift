//
//  ShowApi.swift
//  Jobsity
//
//  Created by Arthur Henrique de Oliveira on 09/05/21.
//

import UIKit

class ShowApi: RestApi {
    
    private static var showByPagePath = "shows?page="
    private static var showByNamePath = "search/shows?q="
    private static var episodeByShowPath = "shows/{id}/episodes"
    
    static func getTvShows(byPage page: Int,
                           success: @escaping ([Show]) -> Void,
                           failure: @escaping (String) -> Void) {
        
        callApi(requestURL: "\(showByPagePath)\(page)",
                method: .get,
                parameters: nil) { (response: [Show]?) in
            if let r = response {
                success(r)
            } else {
                failure("Something went wrong!")
            }
        }
    }
    
    static func getTvShows(byName name: String,
                           success: @escaping ([Show]) -> Void,
                           failure: @escaping (String) -> Void) {
        
        callApi(requestURL: "\(showByNamePath)\(name)",
                method: .get,
                parameters: nil) { (response: [ShowSearch]?) in
            if let r = response {
                
                DispatchQueue.global(qos: .background).async {
                    var shows = [Show]()
                    r.forEach({ search in
                        shows.append(search.show)
                    })
                    DispatchQueue.main.async {
                        success(shows)
                    }
                }
            } else {
                failure("Something went wrong!")
            }
        }
    }
    
    static func getEpisodes(ofShow show: Show,
                            fromSeason season: Int,
                            success: @escaping ([Episode]) -> Void,
                            failure: @escaping (String) -> Void) {
        
        let url = episodeByShowPath.replacingOccurrences(of: "{id}", with: String(show.id!))
        
        callApi(requestURL: url,
                method: .get,
                parameters: nil) { (response: [Episode]?) in
            if let r = response {
                success(r)
            } else {
                failure("Something went wrong!")
            }
        }
    }
}
