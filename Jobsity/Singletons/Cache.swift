//
//  Cache.swift
//  Jobsity
//
//  Created by Arthur Henrique de Oliveira on 12/05/21.
//

import UIKit

class Cache {
    
    private let imageCache = NSCache<NSString, UIImage>()
    
    private static var sharedCache: Cache = {
        let cache = Cache()

        return cache
    }()
    
    static var shared: Cache {
        get {
            sharedCache
        }
    }
    
    func setObject(_ obj: UIImage, forKey key: NSString) {
        imageCache.setObject(obj, forKey: key)
    }
    
    func object(forKey key: NSString) -> UIImage? {
        return imageCache.object(forKey: key)
    }
}
