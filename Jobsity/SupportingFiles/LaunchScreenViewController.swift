//
//  LaunchScreenViewController.swift
//  Jobsity
//
//  Created by Arthur Henrique de Oliveira on 09/05/21.
//

import UIKit
import KeychainSwift

class LaunchScreenViewController: UIViewController {

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if KeychainSwift().get("PIN") == nil {
            let viewController = MenuViewController(nibName: "MenuViewController", bundle: nil)
            let navigationController = UINavigationController(rootViewController: viewController)
            navigationController.modalPresentationStyle = .fullScreen
            present(navigationController, animated: true, completion: nil)
        } else {
            let viewController = InsertPinViewController(nibName: "InsertPinViewController", bundle: nil)
            viewController.modalPresentationStyle = .fullScreen
            present(viewController, animated: true, completion: nil)
        }
    }
}
