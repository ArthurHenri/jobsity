//
//  ViewModelProtocol.swift
//  Jobsity
//
//  Created by Arthur Henrique de Oliveira on 09/05/21.
//

import Foundation

protocol ViewModelDataProtocol {
    func viewModelDidUpdateInfo()
    func viewModelWillStartUpdatingInfo()
    func viewModelDidEndUpdatingInfo()
}
