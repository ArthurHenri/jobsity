//
//  SetPinViewController.swift
//  Jobsity
//
//  Created by Arthur Henrique de Oliveira on 12/05/21.
//

import UIKit

class SetPinViewController: ViewController {

    @IBOutlet weak var pinField: UITextField!
    @IBOutlet weak var touchIdSwitch: UISwitch!
    
    var viewModel = SetPinViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    func setupView() {
        touchIdSwitch.superview?.isHidden = viewModel.shouldHideTouchId
        touchIdSwitch.isOn = viewModel.touchIdSwitchIsOn
    }
    
    override func getTitle() -> String {
        return "PIN"
    }
    
    @IBAction func textFieldChanged() {
        viewModel.pin = pinField.text
    }
    
    @IBAction func saveClicked() {
        if !pinField.text!.isEmpty {
            touchIdSwitch.superview?.isHidden = false
            touchIdSwitch.isOn = false
            viewModel.savePin()
            let alert = UIAlertController(title: "Success", message: "Your PIN has been saved!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            }))
            self.present(alert, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Invalid PIN", message: "You should at least add one number!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func deleteClicled() {
        touchIdSwitch.superview?.isHidden = true
        viewModel.deletePin()
    }
    
    @IBAction func touchIdSwitchChanged() {
        viewModel.changeTouchId(touchIdSwitch.isOn)
    }
}
