//
//  SetPinViewModel.swift
//  Jobsity
//
//  Created by Arthur Henrique de Oliveira on 12/05/21.
//

import Foundation
import KeychainSwift

class SetPinViewModel {
    
    var pin: String?
    var delegate: ViewModelDataProtocol?
    
    var touchIdSwitchIsOn: Bool {
        return KeychainSwift().getBool("TOUCH_ID") != nil
    }
    
    var shouldHideTouchId: Bool {
        return KeychainSwift().get("PIN") == nil
    }
    
    func savePin() {
        KeychainSwift().set(pin!, forKey: "PIN")
    }
    
    func deletePin() {
        KeychainSwift().delete("PIN")
    }
    
    func changeTouchId(_ isOn: Bool) {
        KeychainSwift().set(isOn, forKey: "TOUCH_ID")
    }
}
