//
//  ShowDetailViewController.swift
//  Jobsity
//
//  Created by Arthur Henrique de Oliveira on 09/05/21.
//

import UIKit

class ShowDetailViewController: ViewController {

    @IBOutlet weak var tableView: ShowDetailTableView!
    
    var viewModel: ShowDetailViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func getTitle() -> String {
        return "Details"
    }
    
    private func setupView() {
        tableView.showDetailDataSource = self
        tableView.showDetailDelegate = self
        tableView.reloadData()
        
        viewModel.delegate = self
        viewModel.getEpisodes()
    }
    
    private func updateView() {
        tableView.reloadData()
    }
    
    private func goToDetail(withEpisode episode: Episode) {
        let vm = EpisodeDetailViewModel(withEpisode: episode)
        let vc = EpisodeDetailViewController(nibName: "EpisodeDetailViewController", bundle: nil)
        vc.viewModel = vm
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension ShowDetailViewController: ViewModelDataProtocol {
    func viewModelWillStartUpdatingInfo() {
        startLoading()
    }
    
    func viewModelDidEndUpdatingInfo() {
        stopLoading()
    }
    
    func viewModelDidUpdateInfo() {
        updateView()
    }
}

extension ShowDetailViewController: ShowDetailTableViewDataSource {
    func getNumberOfSeasons() -> Int {
        return viewModel.numberOfSeasons()
    }
    
    func getNumberOfEpisodes() -> Int {
        return viewModel.numberOfEpisodes()
    }
    
    func getShow() -> Show {
        return viewModel.show
    }
    
    func getEpisode(fromIndex index: Int) -> Episode {
        return viewModel.getEpisode(fromIndex: index)
    }
}

extension ShowDetailViewController: ShowDetailTableViewDelegate {
    func didSelectSeason(_ season: Int) {
        viewModel.selectedSeason = season
        updateView()
    }
    
    func didSelectEpisode(fromIndex index: Int) {
        goToDetail(withEpisode: viewModel.getEpisode(fromIndex: index))
    }
}
