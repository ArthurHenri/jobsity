//
//  ShowDetailViewModel.swift
//  Jobsity
//
//  Created by Arthur Henrique de Oliveira on 09/05/21.
//

import Foundation

class ShowDetailViewModel {
    
    var delegate: ViewModelDataProtocol?
    var show: Show!
    var selectedSeason = 1
    
    private var episodes = [Episode]()
    
    func getEpisode(fromIndex index: Int) -> Episode {
        return getEpisodesBySeason()[index]
    }
    
    private func getEpisodesBySeason() -> [Episode] {
        let seasonEpisodes = episodes.filter({ $0.season == selectedSeason })
        return seasonEpisodes
    }
    
    func numberOfEpisodes() -> Int {
        return getEpisodesBySeason().count
    }
    
    func numberOfSeasons() -> Int {
        return episodes.last?.season ?? 1
    }
    
    func getEpisodes() {
        delegate?.viewModelWillStartUpdatingInfo()
        ShowApi.getEpisodes(ofShow: show,
                            fromSeason: 1) { response in
            self.delegate?.viewModelDidEndUpdatingInfo()
            self.episodes = response
            self.delegate?.viewModelDidUpdateInfo()
        } failure: { error in
            self.delegate?.viewModelDidEndUpdatingInfo()
        }
    }
}
