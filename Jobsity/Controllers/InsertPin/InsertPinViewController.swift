//
//  InsertPinViewController.swift
//  Jobsity
//
//  Created by Arthur Henrique de Oliveira on 12/05/21.
//

import UIKit

class InsertPinViewController: ViewController {

    @IBOutlet weak var pinField: UITextField!
    @IBOutlet weak var touchIdButton: UIButton!
    
    var viewModel = InsertPinViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    private func setupView() {
        viewModel.delegate = self
        touchIdButton.isHidden = viewModel.shouldHideTouchId
    }
    
    private func goToMenu() {
        let viewController = MenuViewController(nibName: "MenuViewController", bundle: nil)
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.modalPresentationStyle = .fullScreen
        present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func enterClicked() {
        if viewModel.checkIfPinIsCorrect(pinField.text!) {
            goToMenu()
        } else {
            let alert = UIAlertController(title: "Invalid PIN", message: "The PIN does not match the saved PIN!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func touchIdClicked() {
        viewModel.authenticateUserTouchID()
    }
}

extension InsertPinViewController: InsertPinViewModelDelegate {
    func touchIdSuccess() {
        goToMenu()
    }
}
