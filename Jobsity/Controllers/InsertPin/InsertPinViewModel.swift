//
//  InsertPinViewModel.swift
//  Jobsity
//
//  Created by Arthur Henrique de Oliveira on 12/05/21.
//

import Foundation
import KeychainSwift
import LocalAuthentication

protocol InsertPinViewModelDelegate {
    func touchIdSuccess()
}

class InsertPinViewModel {
    
    var delegate: InsertPinViewModelDelegate?
    
    var shouldHideTouchId: Bool {
        return KeychainSwift().get("TOUCH_ID") == nil
    }
    
    func checkIfPinIsCorrect(_ pin: String) -> Bool {
        return pin == KeychainSwift().get("PIN")
    }
    
    func authenticateUserTouchID() {
        let context : LAContext = LAContext()
        
        var authError: NSError?
        if context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &authError) {
            context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: "Please enter your touch ID.") { success, evaluateError in
                DispatchQueue.main.async {
                    if success {
                        self.delegate?.touchIdSuccess()
                    }
                }
            }
        }
    }
}
