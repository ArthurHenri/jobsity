//
//  MenuViewModel.swift
//  Jobsity
//
//  Created by Arthur Henrique de Oliveira on 09/05/21.
//

import Foundation

class MenuViewModel {
    
    var delegate: ViewModelDataProtocol?
    var searchedName = "" {
        didSet {
            if !searchedName.isEmpty {
                fetchShowsByName()
            }
            delegate?.viewModelDidUpdateInfo()
        }
    }
    
    private var page = -1
    private var shows = [Show]()
    private var searchedShows = [Show]()
    private var isFetching = false
    
    func getShows() -> [Show] {
        if searchedName.isEmpty {
            return shows
        } else {
            return searchedShows
        }
    }
    
    func fetchMorePages()  {
        page += 1
        if !isFetching && searchedName.isEmpty {
            isFetching = true
            fetchShows()
        }
    }
    
    private func fetchShows() {
        delegate?.viewModelWillStartUpdatingInfo()
        ShowApi.getTvShows(byPage: page) { response in
            self.delegate?.viewModelDidEndUpdatingInfo()
            self.shows.append(contentsOf: response)
            self.delegate?.viewModelDidUpdateInfo()
            self.isFetching = false
        } failure: { error in
            self.delegate?.viewModelDidEndUpdatingInfo()
            self.isFetching = false
        }
    }
    
    private func fetchShowsByName() {
        delegate?.viewModelWillStartUpdatingInfo()
        ShowApi.getTvShows(byName: searchedName) { response in
            self.delegate?.viewModelDidEndUpdatingInfo()
            self.searchedShows = [Show]()
            self.searchedShows.append(contentsOf: response)
            self.delegate?.viewModelDidUpdateInfo()
            self.isFetching = false
        } failure: { error in
            self.delegate?.viewModelDidEndUpdatingInfo()
            self.isFetching = false
        }
    }
}
