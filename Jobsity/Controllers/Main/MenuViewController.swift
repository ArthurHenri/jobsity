//
//  MenuViewController.swift
//  Jobsity
//
//  Created by Arthur Henrique de Oliveira on 09/05/21.
//

import UIKit

class MenuViewController: ViewController {
    
    @IBOutlet weak var collectionView: ShowCollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    private let viewModel = MenuViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func getTitle() -> String {
        return "Home"
    }
    
    private func setupView() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "PIN", style: .plain, target: self, action: #selector(pinClicked))
        searchBar.delegate = self
        collectionView.showDelegate = self
        viewModel.delegate = self
        viewModel.fetchMorePages()
    }
    
    private func updateView() {
        collectionView.setData(viewModel.getShows())
    }
    
    @objc private func pinClicked() {
        let vc = SetPinViewController(nibName: "SetPinViewController", bundle: nil)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func goToDetail(withShow show: Show) {
        let vm = ShowDetailViewModel()
        vm.show = show
        let vc = ShowDetailViewController(nibName: "ShowDetailViewController", bundle: nil)
        vc.viewModel = vm
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension MenuViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text!.isEmpty {
            viewModel.searchedName = searchBar.text!
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        viewModel.searchedName = searchBar.text!
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        viewModel.searchedName = ""
    }
}

extension MenuViewController: ViewModelDataProtocol {
    
    func viewModelWillStartUpdatingInfo() {
        startLoading()
    }
    
    func viewModelDidEndUpdatingInfo() {
        stopLoading()
    }
    
    func viewModelDidUpdateInfo() {
        updateView()
    }
}

extension MenuViewController: ShowCollectionViewDelegate {
    func didReachEnd() {
        viewModel.fetchMorePages()
    }
    
    func didSelectShow(_ show: Show) {
        goToDetail(withShow: show)
    }
}
