//
//  EpisodeDetailViewController.swift
//  Jobsity
//
//  Created by Arthur Henrique de Oliveira on 12/05/21.
//

import UIKit

class EpisodeDetailViewController: ViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var episodeNumberLabel: UILabel!
    @IBOutlet weak var summaryLabel: UILabel!
    @IBOutlet weak var episodeImageView: UIImageView!
    
    var viewModel: EpisodeDetailViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateView()
    }
    
    override func getTitle() -> String {
        return "Episode Detail"
    }
    
    func updateView() {
        nameLabel.text = viewModel.name
        episodeNumberLabel.text = viewModel.season
        summaryLabel.text = viewModel.summary
        if let url = viewModel.imageUrl {
            episodeImageView.downloadImage(from: url)
        }
    }
}
