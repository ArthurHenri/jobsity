//
//  EpisodeDetailViewModel.swift
//  Jobsity
//
//  Created by Arthur Henrique de Oliveira on 12/05/21.
//

import Foundation

class EpisodeDetailViewModel {
    
    private var episode: Episode!
    
    var name: String {
        return episode.name ?? ""
    }
    
    var summary: String {
        return episode.summary?.htmlFormat ?? ""
    }
    
    var season: String {
        return "Season: \(episode.season!) | Episode: \(episode.number!)"
    }
    
    var imageUrl: URL? {
        return URL(string: episode.image?.medium ?? "")
    }
    
    init(withEpisode episode: Episode) {
        self.episode = episode
    }
}
