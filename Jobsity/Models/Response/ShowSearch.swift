//
//  ShowSearch.swift
//  Jobsity
//
//  Created by Arthur Henrique de Oliveira on 12/05/21.
//

import UIKit

class ShowSearch: Codable {
    
    var show: Show
}
