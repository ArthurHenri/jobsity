//
//  ShowImage.swift
//  Jobsity
//
//  Created by Arthur Henrique de Oliveira on 09/05/21.
//

import UIKit

class ShowImage: Codable {
    
    var medium: String!
    var original: String!
}
