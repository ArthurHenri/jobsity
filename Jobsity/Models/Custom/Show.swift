//
//  Show.swift
//  Jobsity
//
//  Created by Arthur Henrique de Oliveira on 09/05/21.
//

import UIKit

class Show: Codable {
    var id: Int?
    var name: String?
    var genres: [String]?
    var status: String?
    var image: ShowImage?
    var runtime: Int?
    var rating: Rating?
    var network: Network?
    var schedule: Schedule?
    var summary: String?
    
    func getFormattedGenres() -> String {
        return genres?.joined(separator: ", ") ?? ""
    }
}
