//
//  Rating.swift
//  Jobsity
//
//  Created by Arthur Henrique de Oliveira on 09/05/21.
//

import UIKit

class Rating: Codable {
    var average: Double?
}
