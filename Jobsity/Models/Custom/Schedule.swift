//
//  Schedule.swift
//  Jobsity
//
//  Created by Arthur Henrique de Oliveira on 12/05/21.
//

import UIKit

class Schedule: Codable {
    var time: String!
    var days: [String]!
    
    func getDaysFormated() -> String {
        return days.joined(separator: ", ")
    }
}
