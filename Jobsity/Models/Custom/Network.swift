//
//  Network.swift
//  Jobsity
//
//  Created by Arthur Henrique de Oliveira on 09/05/21.
//

import UIKit

class Network: Codable {
    var id: Int?
    var name: String?
}
