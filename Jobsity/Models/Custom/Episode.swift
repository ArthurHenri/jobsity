//
//  Episode.swift
//  Jobsity
//
//  Created by Arthur Henrique de Oliveira on 12/05/21.
//

import UIKit

class Episode: Codable {
    var id: Int?
    var name: String?
    var season: Int?
    var number: Int?
    var runtime: Int?
    var image: ShowImage?
    var summary: String?
}
