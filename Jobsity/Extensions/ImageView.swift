//
//  ImageView.swift
//  Jobsity
//
//  Created by Arthur Henrique de Oliveira on 12/05/21.
//

import UIKit

extension UIImageView {
    
    func downloadImage(from url: URL, completion: ((UIImage?) -> Void)? = nil) {
        DispatchQueue.global(qos: .background).async {
            if let cachedImage = Cache.shared.object(forKey: url.absoluteString as NSString) {
                DispatchQueue.main.async {
                    self.image = cachedImage
                    
                    if let c = completion {
                        c(cachedImage)
                    }
                }
                
                return
            }
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                
                guard let data = data, error == nil else {
                    DispatchQueue.main.async {
                        
                        if let c = completion {
                            c(nil)
                        }
                    }
                    return
                }
                DispatchQueue.main.async() { [weak self] in
                    if let image = UIImage(data: data) {
                        Cache.shared.setObject(image, forKey: url.absoluteString as NSString)
                    }
                    DispatchQueue.main.async {
                        print(data)
                        self?.image = UIImage(data: data)
                        
                        if let c = completion {
                            c(UIImage(data: data))
                        }
                    }
                }
            }.resume()
        }
    }
}
