//
//  String.swift
//  Jobsity
//
//  Created by Arthur Henrique de Oliveira on 12/05/21.
//

import UIKit

extension String {
    
    var htmlFormat: String? {
        guard let htmlData = data(using: String.Encoding.unicode) else
        {
            return nil
        }
        
        let attributedString = try? NSAttributedString(data: htmlData, options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)

        return attributedString?.string ?? ""
    }
}
